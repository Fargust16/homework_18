﻿#include <iostream>

using namespace std;

class Stack
{
private:
    int maxSize;
    int curSize;
    int* arr;
public:
    Stack() {
        cout << "Enter stack length: ";
        cin >> maxSize;

        curSize = 0;
        arr = new int[maxSize];
    }

    Stack(int stackSize) {
        maxSize = stackSize;
        curSize = 0;
        arr = new int[maxSize];
    }
    ~Stack() {
        delete[] arr;
        curSize = 0;
    }

    int Pop() {
        if (curSize == 0) {
            cout << "Stack is empty" << endl;
            return 0;
        }

        int firstElem = arr[curSize - 1];
        arr[curSize - 1] = arr[curSize];

        curSize--;

        cout << "Pop: " << firstElem << endl;
        return firstElem;
    }

    void Push(int newElem) {
        if (curSize >= maxSize) {
            cout << "Trying to Push: " << newElem << ", but stack is full" << endl;
            return;
        }

        arr[curSize] = newElem;
        curSize++;

        cout << "Push: " << newElem << endl;
    }

    void ToString() {
        if (curSize == 0) {
            cout << "Stack is empty";
            return;
        }

        cout << "Stack: ";

        for (int i = curSize - 1; i >= 0; i--)
        {
            cout << "->" << arr[i];
        }
        cout << endl;
    }
};

int main()
{
    Stack* myStack = new Stack;

    myStack->Push(5);
    myStack->Push(10);
    myStack->Push(15);
    myStack->Pop();
    myStack->Push(25);
    myStack->Push(50);
    myStack->Pop();

    myStack->ToString();
    myStack->Pop();
    myStack->Pop();
    myStack->ToString();

    delete myStack;
}